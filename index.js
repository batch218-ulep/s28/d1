// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// CREATE (Insert Documents)

/*
	SYNTAX: 
			db.collectionName.insertOne({object});
*/

// Insert One
	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "87654321",
			email: "janedoe@mail.com"},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});

// Insert Many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87000",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87894561",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "MongoDB"],
		department: "none"
	}
]);

// To check
	db.users.find();

	db.users.find({firstName: "Stephen"})

// To delete 1
	db.users.deleteOne({
		firstName: "Stephen"
	});

// To delete many
	db.users.deleteMany({
		{

		}
	})


// [SECTION] - READ (Finding documents)
/*
SYNTAX: db.collectionName.find();
SYNTAX: db.collectionName.find({field.value});
SYNTAX: db.collectionName.find({fieldA.valueA, fieldB.valueB...});
*/

// will retrieve all documents
db.users.find();

// will retrieve specific document
db.users.find({firstName: "Stephen"});

// will retrieve multiple documents
db.users.find({lastName: "Armstrong", age: 82});


// [SECTION] UPDATE - updating documents
// syntax: db.collectionName.update({criteria},{$set: {field:value}});
db.users.insertOne({
	firstName: "Test1",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test1"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

db.users.find({firstName: "Bill"});


// UPDATING MULTIPLE DOCUMENTS
// SYNTAX: db.collectionName.updateMany({criteria}, {$set: {field:value}});

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	}
);

db.users.find({department: "HR"});



db.users.updateOne(
	{firstName: "Test1"},
	{
		$set: {
			firstName: "Cely",
			lastName: "Silang",
			age: 66,
			contact: {
				phone: "0999999999",
				email: "silangcely@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "IT",
			status: "inactive"
		}
	}
);

db.users.find({department: "IT"});


// [SECTION] DELETE - deleting docs
/*
single delete: db.collectionName.deleteOne({criteria});
multiple deletion:
*/

// single delete: 
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.deleteOne({
	"_id" : ObjectId("635242837b4af74f5e2d7593")
});


db.rooms.find({
	name: "single"
});



db.users.find({department : "Junior Web Developer"});




// REPLACE - replacing the property itself
db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Mark"
	}
);


// QUERY AN ENBEDDED DOCUMENT
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
});


// QUERY ON NESTED FIELD
db.users.find(
	{"contact.email": "janedoe@mail.com"}
);


// QUERYING AN ARRAY WITH EXACT ELEMENTS
db.users.find(
	{courses: ["CSS", "JavaScript", "Python"]}
	);

// QUERYING AN ARRAY WITHOUT REGARD TO ORDER
db.users.find({courses: {$all: ["CSS", "Python", "JavaScript"]}});

// QUERYING AN EMBEDDED ARRAY

db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});



db.users.find({
	namearr: 
        {
            namea: "juan"
        }
});





